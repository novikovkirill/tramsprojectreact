export default {
	tramModel: 'Tram model',
	quantity: 'Quantity',
	yearsProduced: 'Years produced',
	lowFloor: 'Low floor',
	cardTitle: 'Tram model',
	successMessage: 'Saved succesfully!',
	errorMessage: 'Error! Some fields are missing!'
};