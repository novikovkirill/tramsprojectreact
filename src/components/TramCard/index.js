import React, {Component} from 'react'
import './style.css'

const mesgShowTime = 1000;

class TramCard extends Component {

  state = {
    id: null,
    model: '',
    yearsProduced: '',
    quantity: 1,
    lowFloor: false,
    success: false,
    alertVisible: false
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.tram) this.setState(nextProps.tram)
    else this.setState(TramCard.defaultState)
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

  }

  updateTrams = () => {
    let trams = JSON.parse(localStorage.trams);

    if (this.state.quantity && this.state.yearsProduced && this.state.model){

      trams = trams.map((tram) => {
        if (tram.id === this.state.id) return {
          id: this.state.id,
          model: this.state.model,
          yearsProduced: this.state.yearsProduced,
          quantity: this.state.quantity,
          lowFloor: this.state.lowFloor
        };
        else return tram;
      })
      localStorage.setItem('trams', JSON.stringify(trams));

      this.handleSuccess();
      this.props.onSave();
    } else this.handleError();

  }

  addTram = () => {
    let trams = localStorage.trams ? JSON.parse(localStorage.trams) : [];
    if (this.state.quantity && this.state.yearsProduced && this.state.model){
      const newId = localStorage.idCounter ? +localStorage.idCounter+1 : 0;
      trams.push({
        id: newId,
        model: this.state.model,
        yearsProduced: this.state.yearsProduced,
        quantity: this.state.quantity,
        lowFloor: this.state.lowFloor
      });
      localStorage.setItem('idCounter', newId)
      localStorage.setItem('trams', JSON.stringify(trams));
      this.props.onSave();
      this.props.onClose();
    } else this.handleError();
  }

  handleSuccess = () => {
    this.setState({
      alertVisible: true,
      success: true
    });
    setTimeout(() => {
      this.setState({
        alertVisible: false
      })
    },mesgShowTime)
  }

  handleError = () => {
    this.setState({
      alertVisible: true,
      success: false
    });
    setTimeout(() => {
      this.setState({
        alertVisible: false
      })
    },mesgShowTime)
  }

  render(){

    const showCls = this.props.show ? 'show d-block modal-overlay' : '',
          names = this.props.names,
          tram = this.state,
          visibilityCls = this.state.alertVisible ? 'opaque' : 'transparent',
          alertTypeCls = this.state.success ? 'alert-success' : 'alert-danger',
          alertCls = `alert ${visibilityCls} ${alertTypeCls}`;

    return (
      <div className={"modal fade " + showCls}>
        <div className="modal-dialog modal-dialog-centered ">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{names.cardTitle}</h5>
              <button type="button" onClick={this.props.onClose} className="close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                  <label>{names.tramModel}</label>
                  <input 
                    className="form-control" 
                    id="modelInput"
                    name="model"
                    placeholder={names.tramModel} 
                    value={tram ? tram.model : ''}
                    onChange={this.handleInputChange}
                  />
                </div>
                <div className="form-group">
                  <label>{names.quantity}</label>
                  <input 
                    className="form-control" 
                    id="quantityInput" 
                    name="quantity"
                    placeholder={names.quantity} 
                    value={tram ? tram.quantity : ''}
                    onChange={this.handleInputChange}
                  />
                </div>
                <div className="form-group">
                  <label>{names.yearsProduced}</label>
                  <input 
                    className="form-control" 
                    id="yearsProducedInput" 
                    name="yearsProduced"
                    placeholder={names.yearsProduced} 
                    value={tram ? tram.yearsProduced : ''}
                    onChange={this.handleInputChange}
                  />
                </div>              
                <div className="form-check">
                  <input 
                    type="checkbox" 
                    className="form-check-input" 
                    id="lowFloorInput" 
                    name="lowFloor"
                    checked={tram.lowFloor}
                    onChange={this.handleInputChange}
                  />
                  <label className="form-check-label">{names.lowFloor}</label>
                </div>
              </form>
              <div className={alertCls} style={{transition: 'opacity 1s'}}>
                {this.state.success ? this.props.names.successMessage : this.props.names.errorMessage}
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={this.props.onClose}>Close</button>
              <button 
                type="button" 
                className="btn btn-primary" 
                onClick={this.props.action === 'add' ? this.addTram : this.updateTrams}
              >
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Object.defineProperty(TramCard, 'defaultState', {
    value: {
      id: null,
      model: '',
      yearsProduced: '',
      quantity: 1,
      lowFloor: false,
      success: false,
      error: false
    },
    writable : false,
    enumerable : true,
    configurable : false
});

export default TramCard