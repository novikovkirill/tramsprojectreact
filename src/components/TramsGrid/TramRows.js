import React, { Component } from 'react'
import TramRow from './TramRow'

class TramsRows extends Component {

	render(){ 

		const tramRows = this.props.trams.map((tram) =>
			<TramRow onClick={this.props.onRowClick} onDeleteClick={this.props.onDeleteClick} key={tram.id} tram={tram}/>
		)

		return (
			<tbody>
				{tramRows}
			</tbody>
		)
	}
}

export default TramsRows