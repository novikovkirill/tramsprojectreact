import React, {Component} from 'react'

class TramRow extends Component {

	handleRowClick = (event) => {
		if (event.target.type !== 'button')
 			this.props.onClick(this.props.tram)
	}

	handleDeleteClick = (event) => {
		this.props.onDeleteClick(this.props.tram)
	}

	render () {

		const tram = this.props.tram

		return (
			<tr onClick={this.handleRowClick} style={{cursor: 'pointer'}}>
			    <td>{tram.model}</td>
			    <td>{tram.quantity}</td>
			    <td>{tram.yearsProduced}</td>
			    <td>{tram.lowFloor ? 'Yes' : 'No'}</td>	
			    <td>
			    	<button 
		              type="button" 
		              className="p-2 bd-highlight btn btn-primary" 
		              style={{backgroundColor: '#d80f0f'}} 
		              onClick={this.handleDeleteClick}
		            >
		              Delete
		            </button>
		        </td>		    				    				    
			</tr>
	    )

	}
}

export default TramRow