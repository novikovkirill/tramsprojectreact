import React, { Component } from 'react'
import TramsGridHeader from './TramsGridHeader'
import TramRows from './TramRows'

class TramsGrid extends Component {

	render(){ 

		return (
			<div className="mx-auto" style={{width: '90%'}}>
				<table id="tramsGrid" className="table table-hover mx-auto text-center">
					<TramsGridHeader names={this.props.names}/>
					<TramRows 
						onRowClick={this.props.onRowClick} 
						onDeleteClick={this.props.onDeleteClick} 
						trams={this.props.trams}>
					</TramRows>
				</table>
			</div>
		)
	}
}

export default TramsGrid