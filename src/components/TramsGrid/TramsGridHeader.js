import React from 'react'

export default function TramsGridHeader(props) {

	const names = props.names;

	return (
		<thead>
			<tr>
			    <th>{names.tramModel}</th>
			    <th>{names.quantity}</th>
			    <th>{names.yearsProduced}</th>
			    <th>{names.lowFloor}</th>
			    <th></th>
		    </tr>			    				    				    
		</thead>
    )

}