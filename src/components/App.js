import React, { Component } from 'react'
import TramsGrid from './TramsGrid'
import TramCard from './TramCard'
import names from '../locales/locale_en'

//TODO: filters, pagination

class App extends Component {

  state = {
    cardAction: 'add',
    showCard: false,
    trams: localStorage.trams ? JSON.parse(localStorage.trams) : [],
    openedTramId: null,
  }

  render() {

    const openedTram = this.state.trams.find((tram) => {
      return this.state.openedTramId === tram.id;
    });

    return (
      <div>
        <div className="jumbotron" style={{backgroundColor: '#35acf2'}}>
          <h1 className="text-center display-3">Trams list</h1>
        </div>
        <div className="d-flex flex-row bd-highlight mb-3 justify-content-around ml-3" style={{width: '15%'}}>
            <button 
              type="button" className="p-2 bd-highlight btn btn-primary"  
              style={{backgroundColor: '#3d8240'}} 
              onClick={this.onAddClick}>Add
            </button>
        </div>
        <div style={{maxHeight: '450px', overflowY: 'auto'}}>
        <TramsGrid onRowClick={this.onRowClick} onDeleteClick={this.onDeleteClick} names={names} trams={this.state.trams}/>
        </div>
        <TramCard 
          action={this.state.cardAction}
          onClose={this.onCardClose} 
          onSave={this.onCardSave} 
          names={names} 
          show={this.state.showCard} 
          tram={openedTram}>
        </TramCard>
      </div>
    );
  }

  onAddClick = (tram) => {
    this.setState({
      cardAction: 'add',
      showCard: true,
      openedTramId: null
    })
  }

  onDeleteClick = (clickedTram) => {
    const tramId = clickedTram.id;
    let trams = JSON.parse(localStorage.trams);
    trams = trams.filter((tram) => {
      return tram.id !== tramId;
    })
    localStorage.setItem('trams', JSON.stringify(trams));
    this.setState({
      trams: JSON.parse(localStorage.trams),
    });
  }

  onRowClick = (tram) => {
      this.setState({
        cardAction: 'edit',
        showCard: true,
        openedTramId: tram.id
      });
  }

  onCardClose = () => {
    this.setState({
      showCard: false
    });
  }

  onCardSave = () => {
    this.setState({
      trams: JSON.parse(localStorage.trams),
    });
  }

}

export default App